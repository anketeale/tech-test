# payl8r-technical-test-FE

You should find a JSON array containing a list of retailers.
We would like you to do the following.

- Using HTML, CSS and JavaScript display this list so we can view all of the shops.
- Sort the list alphabetically, (this should be the default).
- Be able to categorize the the list by industry

Feel free to use a a framework of your choosing, please try to steer clear of any theming frameworks e.g. Bootstrap/jQuery plugins.
Preprocessors and build tools are fine to use but please include these in the project.

Things we like

- Clean and semantic markup
- Modular css/scss - i.e. (flexbox / grid)
- Easy to read code
- Code written in ES6 +
- Bonus points for any creative flair

The purpose of this test is to gauge your understanding ES6 native APIs along with how that fits with your chosen JS framework (e.g. React, VueJS, Angular).

Your approach will be used as talking points within the interview. Have fun and show off your creative chops.
